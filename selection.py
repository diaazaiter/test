from alexa import  queue, logger, api
from music import MusicWrapper
from random import randint
import alexa
import requests


def login():
    text = 'Welcome to Anghami. \
            Try asking me to play a song or start a playlist'
    prompt = 'For example say, play songs by Adele'
    image_url = {
            'small': 'https://s3-eu-west-1.amazonaws.com/anghami.intern/diaa/icons/anghami-welcome-small.jpeg',
            'large': 'https://s3-eu-west-1.amazonaws.com/anghami.intern/diaa/icons/anghami-welcome-large.jpeg'
            }
    return alexa.build_response({}, alexa.build_speechlet_response("welcome", text, prompt, image_url, False))


def help():
    text = ''' Here are some things you can say:
                Play songs by rihanna,
                Play the album hello by Adele,
                Play the song impossible,
                play my hits playlist,
                play music from my downloads,
                and play music from my likes,

                Of course you can also say skip, previous, shuffle, and more
                of alexa's music commands or, stop, if you're done.
                Remember to start your request by saying: Alexa, ask anghami music.
                So, what can I do for you?
                '''

    prompt = 'For example say, play songs by Adele'
    session_attributes = {}
    card_title = "Help"
    image_url = {
            'small': 'https://s3-eu-west-1.amazonaws.com/anghami.intern/diaa/icons/help-small.jpeg',
            'large': 'https://s3-eu-west-1.amazonaws.com/anghami.intern/diaa/icons/help-large.jpeg'
            }
    return alexa.build_response(session_attributes, alexa.build_speechlet_response(
        card_title, text, prompt, image_url, False))



def play_artist(token, artist_name):
    # Fetch the artist
    artist = api.get_artist_songs(token, artist_name)

    if artist is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find that artist"))

    # Setup the queue
    first_song_id = queue.reset(artist)

    # Get a streaming URL for the top song
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))

    image_url = api.get_thumbnail(artist[0]['AlbumArt'])
    speech_text = "Playing top tracks by %s" % artist[0]['artist']
    # print stream_url
    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(artist[0]['title'], artist[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))



def play_album(token, album_name, artist_name = None):
    logger.debug("Fetching album %s by %s" % (album_name, artist_name))

    # Fetch the album
    album = api.get_album(token, album_name, artist_name)
    if album is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find that album"))

    # Setup the queue
    first_song_id = queue.reset(album['sections'][0]['data'])

    # Start streaming the first track
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))


    speech_text = "Playing album %s by %s" %(album['title'], album['artist'])
    image_url = api.get_thumbnail(album['coverArt'])

    return alexa.build_response(audio_response = alexa.build_audio_response(album['title'], image_url, speech_text,
        stream_url, first_song_id))

def play_song(token, song_name, artist_name = None, speech_text = None):
    logger.debug("Fetching song %s by %s" % (song_name, artist_name))

    # Fetch the song
    song = api.get_song(token, song_name, artist_name)

    if song is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find that song"))

    # Start streaming the first track
    first_song_id = queue.reset(song)
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))

    image_url = api.get_thumbnail(song[0]['AlbumArt'])
    if speech_text is None:
        speech_text = "Playing %s by %s" % (song[0]['title'], song[0]['artist'])

    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(song[0]['title'], song[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))



def play_likes(token):
    likesid, likes = api.get_likes(token)
    if likes is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, you don't have any likes"))

    for val in likes:
        val['id'] = "%sP%s" %(likesid, val['id'])

    first_song_id = queue.reset(likes)

    # Get a streaming URL for the first song in the playlist
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))

    speech_text = "Playing songs from your likes"
    image_url = api.get_thumbnail(likes[0]['AlbumArt'])

    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(likes[0]['title'], likes[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))

def play_downloads(token):
    downloadsid, downloads = api.get_downloads(token)
    if downloads is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, you don't have any donwloads"))

    for val in downloads:
        val['id'] = "%sP%s" %(downloadsid, val['id'])

    first_song_id = queue.reset(downloads)

    # Get a streaming URL for the first song in the playlist
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))

    speech_text = "Playing songs from your downloads"
    image_url = api.get_thumbnail(downloads[0]['AlbumArt'])

    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(downloads[0]['title'], downloads[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))

def play_new_songs(token, language = None):

    language, newsongs = api.get_new_songs(token, language)
    first_song_id = queue.reset(newsongs)
    # Get a streaming URL for the first song in the playlist
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))
    speech_text = "Playing new songs"
    if language is not None:
        speech_text = "Playing new %s songs" %(language)
    image_url = api.get_thumbnail(newsongs[0]['AlbumArt'])
    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(newsongs[0]['title'], newsongs[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))


def play_playlist(token, playlist_name):
    # Retreve the content of all playlists in a users library
    playlistid, playlist = api.get_playlist(token, playlist_name)

    if playlist is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find that playlist."))

    # Add songs from the playlist onto our queue
    for val in playlist:
        val['id'] = "%sP%s" %(playlistid, val['id'])
    first_song_id = queue.reset(playlist)

    # Get a streaming URL for the first song in the playlist
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))

    speech_text = "Playing songs from %s" % (playlist_name)
    image_url = api.get_thumbnail(playlist[0]['AlbumArt'])

    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(playlist[0]['title'], playlist[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))






def queue_song(token, song_name, artist_name = None):
    logger.debug("Queuing song %s by %s" % (song_name, artist_name))
    if song_name is None:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, you need to specify a song."))

    if len(queue.song_ids) == 0:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "No queue", "Sorry, you must first play a song"))

    # Fetch the song
    song = api.get_song(token, song_name, artist_name)

    if song is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find that song"))

    # Queue the track in the list of song_ids
    queue.insert_track(song)
    stream_url = api.get_stream_url(token, song[0]['id'])
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))

    speech_text = "Queued %s by %s." % (song[0]['title'], song[0]['artist'])

    return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
        "Queued", speech_text))



def list_albums_by_artist(token, artist_name):
    if artist_name is None:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, you need to specify the artist name."))
    artist_album_list = api.get_artist_album_list(token, artist_name=artist_name)
    return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "album list", artist_album_list))


def list_latest_album_by_artist(token, artist_name):
    if artist_name is None:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, you need to specify the artist name."))
    try:
        title, latest_release = api.get_latest_release(token, artist_name=artist_name)
    except:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, %s doesn't have any recent releases" % artist_name))
    return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "latest album", "%s's latest album is: %s" %(latest_release['artist'], latest_release['title'])))



def play_latest_release_by_artist(token, artist_name):

    if artist_name is None:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, you need to specify the artist name."))
    try:
        title, latest_release = api.get_latest_release(token, artist_name)
    except:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, %s doesn't have any recent releases" % artist_name))
    if title == 'Latest Album':
        latest_album = api.get_album_by_id(token, latest_release['id'])
        if latest_album is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Not found", "Sorry, I couldn't find any album to play"))

        # Setup the queue
        first_song_id = queue.reset(latest_album)

        # Start streaming the first track
        stream_url = api.get_stream_url(token, first_song_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))


        speech_text = "Playing latest album: %s by %s" % (latest_album[0]['album'], latest_album[0]['artist'])
        image_url = api.get_thumbnail(latest_album[0]['AlbumArt'])

        return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(latest_album[0]['title'], latest_album[0]['artist']), image_url, speech_text,
        stream_url, first_song_id))
    else:
        speech_text = "Playing latest song: %s by %s" % (latest_release['title'], latest_release['artist'])
        return play_song(token, latest_release['title'], latest_release['artist'], speech_text)



def play_album_by_artist(token, artist_name):

    album = api.get_album_by_artist(token, artist_name=artist_name)

    if album is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find any albums"))

    # Setup the queue
    first_song_id = queue.reset(album['sections'][0]['data'])

    # Start streaming the first track
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))


    speech_text = "Playing album %s by %s" % (album['title'], album['artist'])
    image_url = api.get_thumbnail(album['coverArt'])


    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(album['sections'][0]['data'][0]['title'], album['artist']), image_url, speech_text,
        stream_url, first_song_id))


def play_different_album(token):


    current_track = queue.current_track()

    if current_track is None:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, there are no album playing currently"))

    album = api.get_album_by_artist(token, artist_name=current_track['artist'], album_id=current_track['albumID'])

    if album is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Not found", "Sorry, I couldn't find any albums"))

    # Setup the queue
    first_song_id = queue.reset(album['sections'][0]['data'])

    # Start streaming the first track
    stream_url = api.get_stream_url(token, first_song_id)
    if stream_url == "Plus":
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
    elif stream_url is False:
        return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
            "Error", "Sorry, something went wrong, please try again later."))


    speech_text = "Playing album %s by %s" % (album['title'], album['artist'])
    image_url = api.get_thumbnail(album['coverArt'])

    return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(album['sections'][0]['data'][0]['title'], album['artist']), image_url, speech_text,
        stream_url, first_song_id))
