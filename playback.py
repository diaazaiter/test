from flask_ask import statement, audio
from alexa import  queue, logger, api
from music import MusicWrapper
from builtins import object
import selection
import alexa
    ##
    # Callbacks
    #

class playback(object):
    def __init__(self, queue):
        self.reset()

    def reset(self):
        self.offset = 0
        self.loop = False
        self.stopp = False
        self.queued = False
        self.shuffle = False
        self.radioData = {}
        self.radio = False
        self.radioName = ""
        self.radioId = ""

    def stopped(self, access_token, token, offset):
        self.offset = offset
        logger.debug("Stopped at %s" % offset)
        self.send_song_data(access_token, token, offset)

    def started(self, offset):
        self.stopp = False
        self.queued = False
        logger.debug("Started at %s" % offset)



    def nearly_finished(self, token):
        current_id = queue.current()
        next_id = queue.up_next()
        if self.loop:
            next_id = current_id
        if next_id is not None:
            self.queued = True
            logger.debug("Queued")
            stream_url = api.get_stream_url(token, next_id)
            if self.shuffle:
                self.shuffle = False
                return
            return alexa.build_queue_response(stream_url, next_id, "ENQUEUE", current_id)


    def finished(self, access_token, token):
        if self.loop:
            logger.debug("Finished and looping")
            self.send_song_data(access_token, token)
            return None
        queue.next()
        logger.debug("Finished")
        self.send_song_data(access_token, token)


    ##
    # Intents
    #



    def start_over(self, token):
        next_id = queue.current()

        if next_id is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, there are no songs on the queue"))
        else:
            stream_url = api.get_stream_url(token, next_id)
            if stream_url == "Plus":
                return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                    "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
            elif stream_url is False:
                return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                    "Error", "Sorry, something went wrong, please try again later."))


            return alexa.build_response(audio_response = alexa.build_audio_response("", "", "",
                stream_url, next_id))



    def resume(self, token):
        current_id = queue.current()

        if current_id is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, you must play a song first"))

        stream_url = api.get_stream_url(token, current_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))

        logger.debug("resumed at offset: %s" % self.offset)
        if self.stopp:
            self.stopp = False
            self.offset = 0
            logger.debug("resume offset updated to %s after stop command" % self.offset)

        return alexa.build_response(audio_response = alexa.build_audio_response("", "", "",
            stream_url, current_id, offset = self.offset))



    def pause(self, token):
        current_id = queue.current()

        if current_id is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, you must play a song first"))
        else:
            logger.debug("Paused")
            stream_url = api.get_stream_url(token, current_id)
            if stream_url == "Plus":
                return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                    "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
            elif stream_url is False:
                return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                    "Error", "Sorry, something went wrong, please try again later."))

            return alexa.build_response(audio_response = alexa.build_audio_response("", "", "",
                stream_url, current_id, type = "AudioPlayer.Stop", behavior = {}))



    def stop(self, token):
        self.stopp = True
        current_id = queue.current()

        if current_id is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Good Bye", "Nice talking to you. Come back soon."))
        else:
            logger.debug("Stopped")
            stream_url = api.get_stream_url(token, current_id)
            if stream_url == "Plus":
                return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                    "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
            elif stream_url is False:
                return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                    "Error", "Sorry, something went wrong, please try again later."))
            queue.reset()
            return alexa.build_response(audio_response = alexa.build_audio_response("", "", "Nice conversation. Goodbye!",
            stream_url, current_id, type = "AudioPlayer.Stop"))




    def next_song(self, token):
        next_id = queue.next()

        if next_id is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, there are no songs on the queue"))

        stream_url = api.get_stream_url(token, next_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))

        song = queue.current_track()
        image_url = api.get_thumbnail(song['AlbumArt'])


        return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(song['title'], song['artist']), image_url, "",
                stream_url, next_id))




    def prev_song(self, token):
        prev_id = queue.prev()

        if prev_id is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, you can't go back any farther in the queue"))

        stream_url = api.get_stream_url(token, prev_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))

        song = queue.current_track()
        image_url = api.get_thumbnail(song['AlbumArt'])


        return alexa.build_response(audio_response = alexa.build_audio_response("%s by %s" %(song['title'], song['artist']), image_url, "",
                stream_url, prev_id))



    def shuffle_on(self, token):

        if len(queue.song_ids) == 0:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, there are no songs to shuffle"))

        current_id = queue.shuffle_mode(True)
        next_id = queue.up_next()
        stream_url = api.get_stream_url(token, next_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))


        if self.queued:
            logger.debug("shuffle ON; Queued!")
            return alexa.build_queue_response(stream_url, next_id, "REPLACE_ENQUEUED")
        self.shuffle = True
        logger.debug("shuffle ON; Not Queued yet!")
        return alexa.build_queue_response(stream_url, next_id, "ENQUEUE", current_id)
        # return alexa.build_response(audio_response = alexa.build_audio_response("",
        #     stream_url, current_id, offset = self.paused_offset))



    def shuffle_off(self, token):
        if len(queue.song_ids) == 0:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, there are no songs to unshuffle"))


        current_id = queue.shuffle_mode(False)
        next_id = queue.up_next()
        stream_url = api.get_stream_url(token, next_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))

        if self.queued:
            logger.debug("shuffle OFF; Queued!")
            return alexa.build_queue_response(stream_url, next_id, "REPLACE_ENQUEUED")
        self.shuffle = True
        logger.debug("shuffle OFF; Not Queued yet!")
        return alexa.build_queue_response(stream_url, next_id, "ENQUEUE", current_id)
        # return alexa.build_response(audio_response = alexa.build_audio_response("",
        #     stream_url, current_id, offset = self.paused_offset))


    def loop_on(self, token):
        if len(queue.song_ids) == 0:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, there are no songs in the queue"))

        self.loop = True
        current_id = queue.current()
        stream_url = api.get_stream_url(token, current_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))


        if self.queued:
            return alexa.build_queue_response(stream_url, current_id, "REPLACE_ENQUEUED")
        return alexa.build_queue_response(stream_url, current_id, "ENQUEUE", current_id)
        # return alexa.build_response(audio_response = alexa.build_audio_response("",
        #     stream_url, current_id, offset = self.paused_offset))
        # response = alexa.build_response(audio_response = alexa.build_audio_response("",
        #     stream_url, current_id, behavior = {"playBehavior": "REPLACE_ENQUEUED"}))
        # previous = {"expectedPreviousToken": current_id}
        # response['response']['directives'][0]['audioItem']['stream'].update(previous)
        # del response['response']['card']
        # del response['response']['outputSpeech']
        # return response


    def loop_off(self, token):
        if len(queue.song_ids) == 0:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No songs", "Sorry, there are no songs in the queue"))

        self.loop = False
        current_id = queue.current()
        next_id = queue.up_next()
        stream_url = api.get_stream_url(token, next_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))


        if self.queued:
            return alexa.build_queue_response(stream_url, next_id, "REPLACE_ENQUEUED")
        return alexa.build_queue_response(stream_url, next_id, "ENQUEUE", current_id)


        # response =  alexa.build_response(audio_response = alexa.build_audio_response("",
        #     stream_url, next_id, behavior = {"playBehavior": "REPLACE_ENQUEUED"}))
        # previous = {"expectedPreviousToken": current_id}
        # response['response']['directives'][0]['audioItem']['stream'].update(previous)
        # del response['response']['card']
        # del response['response']['outputSpeech']
        # return response


    def currently_playing(self):
        track = queue.current_track()

        if track is None:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Nothing playing", "Nothing is playing right now"))

        image_url = api.get_thumbnail(track['AlbumArt'])
        output = "The current track is %s by %s" % (track['title'], track['artist'])
        response = alexa.build_speechlet_response("%s by %s" % (track['title'], track['artist']),
            output, image_url = image_url)
        return alexa.build_response(speechlet_response = response)


    def restart_tracks(self, token):
        if len(queue.song_ids) == 0:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "No Songs", "You must first play tracks to restart them"))

        queue.current_index = 0
        current_id = queue.current()
        stream_url = api.get_stream_url(token, current_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))

        return alexa.build_response(audio_response = alexa.build_audio_response("", "", "Restarting tracks",
            stream_url, current_id))


    # https://github.com/stevenleeg/geemusic/issues/28
    def skip_to(self, token, song_name = None, artist_name = None):
        if song_name is None:
            return selection.play_artist(token, artist_name)
        return selection.play_song(token, song_name, artist_name)
            # .standard_card(title=speech_text,
            #                text='',
            #                small_image_url=thumbnail,
            #                large_image_url=thumbnail)




    def request_radio(self, token, radio_name = None,  data = []):
        data = []
        if radio_name is not None:
            radio = api.get_radio_by_name(token, radio_name, data)
        else:
            radio = api.get_random_radio(token, data)
        if radio is False:
            return False

        radioSongs = radio['sections'][0]['data']
        radioId = radio['radioID']
        radioName = radio['radioName']
        for val in radioSongs:
            x = {"songid": val['id']," artistid": val['artistID'],"duration": val['duration'], "radioName": radioName}
            data.append(x)
            val['id'] = "%s/%s" %(radioId, val['id'])
        self.radioData[radioId] = data
        self.radioData["%s//" %(radioId)] = []
        return radioName, radioSongs


    def play_radio(self, token, radio_name = None):
        try:
            radioName, radioSongs = self.request_radio(token, radio_name)
        except:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Radio Not Found", "Sorry, radio not found!"))
        first_song_id = queue.reset(radioSongs)
        stream_url = api.get_stream_url(token, first_song_id)
        if stream_url == "Plus":
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Anghami Plus", "This song is exclusive to Anghami Plus. Subscribe to unlock."))
        elif stream_url is False:
            return alexa.build_response(speechlet_response = alexa.build_speechlet_response(
                "Error", "Sorry, something went wrong, please try again later."))

        self.radioName = "####"
        speech_text = "Playing %s radio" % (radioName)
        image_url = api.get_thumbnail(radioSongs[0]['AlbumArt'])

        return alexa.build_response(audio_response = alexa.build_audio_response("%s radio" % (radioName), image_url,
            speech_text, stream_url, first_song_id))

    def send_song_data(self, access_token, token, offset = None):
        if "/" in token:
            self.radio = True
            if self.radioName == "####" and self.radioId != "":
                self.radioName = self.radioData[self.radioId][0]['radioName']
                self.request_radio(access_token, self.radioName, self.radioData["%s//" %(self.radioId)])
                logger.debug("previous radio data sent; Id: %s" %self.radioId)

            self.radioId = token[:token.index("/")]
            songId = token[token.index("/")+1:]
            self.radioName = self.radioData[self.radioId][0]['radioName']
            found = False
            for song in self.radioData["%s//" %(self.radioId)]:
                if song['songid'] == songId:
                    found = True
                    if offset is not None:
                        playper = ((float(offset)/1000.0)/float(song['duration'])) * 100.0
                    else:
                        playper = 100.0
                    if float(song['playper']) < playper:
                        song['playper'] = playper
                        logger.debug("radio song data: %s" %song)
                    break
            if found is False:
                for song in self.radioData[self.radioId]:
                    if song['songid'] == songId:
                        if offset is not None:
                            playper = ((float(offset)/1000.0)/float(song['duration'])) * 100.0
                        else:
                            playper = 100.0
                        x = {"songid": song['songid']," artistid": song['artistid'],"playper": playper}
                        self.radioData["%s//" %(self.radioId)].append(x)
                        logger.debug("radio song data: %s" %x)
                        break
        elif "P" in token:
            playlistId = token[:token.index("P")]
            songId = token[token.index("P")+1:]
            duration, extras = api.get_info(access_token, songId)
            if offset is not None:
                playper = ((float(offset)/1000.0)/float(duration))
                playsecs = float(offset)/1000.0
            else:
                playper = 1
                playsecs = float(duration)
            api.register_web_play(access_token, songId, playper, playsecs, extras, playlistId)


        else:
            if self.radio:
                self.radio = False
                self.request_radio(access_token, self.radioName, self.radioData["%s//" %(self.radioId)])
                logger.debug("radio data sent; Id: %s" %self.radioId)
                self.radioId = ""
            else:
                duration, extras = api.get_info(access_token, token)
                if offset is not None:
                    playper = ((float(offset)/1000.0)/float(duration))
                    playsecs = float(offset)/1000.0
                else:
                    playper = 1
                    playsecs = float(duration)
                api.register_web_play(access_token, token, playper, playsecs, extras)



    def session_ended(self):
        return "", 200
