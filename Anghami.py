import requests, json
from builtins import object
import hashlib
import urllib2
from pprint import pprint
import time

# sdvmreivbntribndeendvkrvki
# curl -X Post -H "Host: apilexa.anghami.com" -H "Content-Type: application/x-www-form-urlencoded" -H "User-Agent: Anghami/2.9.9 CFNetwork/711.1.12 Darwin/14.0.0 iPad2,5 v2 alexa" -H "X-ANGH-RQSIG: 78c28383fd2d3a0b7147d52b4768befd" --data "token=i7:eficclkh::ecdjcjecckdegc:YO:fe:ra:v2.9.9:112::v2.9.9:0:na:9oorpr8772&query=melhem zein&searchtype=artist&output=jsonhp" "https://apilexa.anghami.com/rest/do/GETtabsearch"

# curl -X Post -H "Host: apilexa.anghami.com" -H "Content-Type: application/x-www-form-urlencoded" -H "User-Agent: Anghami/2.9.9 CFNetwork/711.1.12 Darwin/14.0.0 iPad2,5 v2 alexa" -H "X-ANGH-RQSIG: b2ee4505725724eb5abcb3f52d86ecb5" --data "token=8c3daf372f1d57212cde6c63f346c655&artistid=680&language=en&output=jsonhp" "https://apilexa.anghami.com/rest/do/GETartistprofile"


class Anghami(object):
    def __init__(self):
        self._url = "https://apilexa.anghami.com/rest/do/"
        self.key = "qg43RuwbqTkxTvey"
        self.headers = {'Host': 'apilexa.anghami.com','Content-Type': 'application/x-www-form-urlencoded','User-Agent': 'Anghami/2.9.9 CFNetwork/711.1.12 Darwin/14.0.0 iPad2,5 v2 alexa'}


    # searchType = {top, artist, song, playlist, album, user}
    def getTabSearch(self, token, query, searchType):
        url = self._url + "GETtabsearch"
        data = 'token=%s&query=%s&searchtype=%s&output=jsonhp' %(token, query, searchType)
        uri = "/rest/do/GETtabsearch"
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        return json_response['sections']

    def getArtistProfile(self, token, artistid, language = "en"):
        url = self._url + "GETartistprofile"
        uri = "/rest/do/GETartistprofile"
        data = 'token=%s&artistid=%s&language=%s&output=jsonhp' %(token, artistid, language)
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        return json_response

    def getArtistSongs(self, token, artistid, language = "en"):
        url = self._url + "GETartistsongs"
        uri = "/rest/do/GETartistsongs"
        data = 'token=%s&artistid=%s&language=%s&output=jsonhp' %(token, artistid, language)
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        return json_response

    def getSong(self, token, songid, extras = None):
        url = self._url + "GETsong"
        uri = "/rest/do/GETsong"

        data = 'token=%s&songid=%s&output=jsonhp' %(token, songid)
        if extras is not None:
            data += '&extras=%s' %(extras)
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        return json_response['sections']

    def getPlaylists(self, token):
        url = self._url + "GETplaylists"
        uri = "/rest/do/GETplaylists"

        data = 'token=%s&output=jsonhp' %(token)
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        return json_response['sections']



    def getPlaylistData(self, token, playlistid, language = 'en'):
        url = self._url + "GETplaylistdata"
        uri = "/rest/do/GETplaylistdata"

        data = 'token=%s&playlistid=%s&language=%s&output=jsonhp' %(token, playlistid, language)
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        if len(json_response['sections']) == 0:
            return False
        return json_response['sections'][0]['data']

    def getAlbumData(self, token, albumid, language = "en"):
        url = self._url + "GETalbumdata"
        uri = "/rest/do/GETalbumdata"
        data = 'token=%s&albumid=%s&language=%s&output=jsonhp' %(token, albumid, language)

        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers=self.headers, data=data)
        json_response = json.loads(r.text)
        try:
            json_response['id']
        except:
            return False
        return json_response

    def getTrack(self, token, songid):
        url = self._url + 'GETdownload'
        uri = "/rest/do/GETdownload"
        data = 'token=%s&output=json&fileid=%s' %(token, songid)

        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers = self.headers, data = data)
        json_response = json.loads(r.text)
        if 'error' in json_response:
            if json_response['error']['_attributes']['code'] == 35:
                return 'Plus'
            else:
                return False
        return json_response['song']['_attributes']

    def getGenericContent(self, token, gcid = 'new_music', language = 0):
        url = self._url + 'GETgenericcontent'
        uri = '/rest/do/GETgenericcontent'
        data = 'token=%s&output=jsonhp&generic_content_id=%s&musiclanguage=%s' %(token, gcid, language)

        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers = self.headers, data = data)
        json_response = json.loads(r.text)
        req = json_response['buttons'][0]['deeplink']
        # return req
        req = req.replace('anghami://api/v1/GETsong.view', '')
        data = urllib2.unquote(req)[1:]
        songId = data[data.index('songid=')+7: data.index('&extras')]
        extras =  data[data.index('extras=')+7:]
        return songId, extras
        return self.getSong(token, songId, extras)[1]['data']


    # def getTrack(self, token, songid):
    #     url = self._url + "GETtrack?songid=%s" %(songid)
    #
    #     r = requests.head(url)
    #
    #     stream_url = r.headers['Location']
    #     return stream_url
    #

    def getRadios(self, token):
        url = self._url + 'GETradios'
        uri = "/ret/do/GETradios"
        data = "token=%s&output=jsonhp" %(token)

        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers = self.headers, data = data)
        json_response = json.loads(r.text)
        return json_response

    def getRadioData(self, token, radioType, id, rdata = []):
        url = self._url + 'Radio'
        uri = "/rest/do/Radio"
        data = "token=%s&output=jsonhp&radiotype=%s&id=%s" %(token, radioType, id)
        if rdata != []:
            data += "&data=%s" % rdata

        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers = self.headers, data = data)
        json_response = json.loads(r.text)
        return json_response

    def getDisplayTags(self, token):
        url = self._url + 'GETdisplaytags'
        uri = '/rest/do/GETdisplaytags'
        data = "token=%s&output=jsonhp" %(token)
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers = self.headers, data = data)
        json_response = json.loads(r.text)
        for val in json_response['sections']:
            if val['group'] == 'tag':
                return val['data']
        return None


    def registerWebPlay(self, token,songid, playper, playsecs, extras, playlistid = None):
        url = self._url + 'REGISTERwebplay'
        uri = '/rest/do/REGISTERwebplay'
        data = "token=%s&songid=%s&playper=%s&playsecs=%s&localtimestamp=%s&extras=%s&alexa=1&output=jsonhp" %(token, songid, playper, playsecs, int(time.time()*1000), extras)
        if playlistid is not None:
            data += "&playlistid=%s" %playlistid
        sig = self.generateSignature(self.preparedRequest(uri, data, self.key))
        self.headers.update(sig)
        r = requests.post(url, headers = self.headers, data = data)
        json_response = json.loads(r.text)
        return json_response

    def getThumbnail(self, id, size = 720):
        url = "https://anghamicoverart.akamaized.net/?id=%s&size=%s&cachebuster=alexa1233" %(id, size)
        # url = "https://newart.anghami.com/?id=%s&size=%s&1223" %(id, size)

        return url

    def generateSignature(self, preparedRequest):
        toBeHashed = preparedRequest['uri'] + preparedRequest['data'] + preparedRequest['key']
        m = hashlib.sha1()
        m.update(toBeHashed)
        hashedHex = m.hexdigest()
        return { 'X-ANGH-RQSIG': hashedHex[0:32]}


    def preparedRequest(self, uri, data, key):
        return {'uri': uri, 'data': data, 'key': key}
