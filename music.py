from builtins import object
from fuzzywuzzy import fuzz
import threading, json
import random

from Anghami import Anghami


class MusicWrapper(object):
    def __init__(self, logger=None):
        self._api = Anghami()
        self.logger = logger




    def _search(self, token, query_type, query):

        if query_type == 'track':
            query_type = 'song'

        results = self._api.getTabSearch(token, query, query_type)[0]['data']

        return results

    def get_tag_id(self, token, tag_name):
        search = self._api.getTabSearch(token, tag_name, "top")
        for val in search:
            if val['type'] == 'genericitem':
                for item in val['data']:
                    if item['generictype'] == 'tag':
                        return item['id']
        return False

    def get_artist(self, token, name):
        """
        Fetches information about an artist given its name
        """
        search = self._search(token, "artist", name)

        if len(search) == 0:
            return False

        return self._api.getArtistProfile(token, search[0]['id'],
                                         'en')

    def get_artist_songs(self, token, name):
        search = self._search(token, "artist", name)

        if len(search) == 0:
            return False

        return self._api.getArtistSongs(token, search[0]['id'],
                                         'en')['sections'][0]['data']

    def get_album_by_id(self, token, album_id):
        try:
            return self._api.getAlbumData(token, album_id, 'en')['sections'][0]['data']
        except:
            return False

    def get_album(self, token, name, artist_name=None):
        if artist_name:
            name = "%s %s" % (name, artist_name)

        search = self._search(token, "album", name)

        if len(search) == 0:
            return False

        return self._api.getAlbumData(token, search[0]['id'])

    def get_latest_release(self, token, artist_name):
        release =  self.get_artist(token, artist_name)['sections']
        for val in release:
            if val['group'] == 'latestsong':
                return val['title'], val['data'][0]
        return False

    def get_album_by_artist(self, token, artist_name, album_id=None):


        artist_info = self.get_artist(token, artist_name)['sections']
        album_info = []
        for val in artist_info:
            if val['group'] == 'albums':
                album_info = val['data']
                break
        if album_info != []:
            random.shuffle(album_info)
            for index, val in enumerate(album_info):
                if album_info[index]['id'] != album_id:
                    album = self._api.getAlbumData(token, album_info[index]['id'], 'en')
                    return album
        return False




    def get_song(self, token, name, artist_name=None):
        if artist_name:
            name = "%s %s" % (artist_name, name)

        search = self._search(token, "song", name)

        if len(search) == 0:
            return False

        songId = search[0]['id']

        songlist = self._api.getSong(token, songId)[0]['data'] + self._api.getSong(token, songId)[1]['data']
        return songlist

    def get_song_by_id(self, token, song_id):
        if "/" in song_id:
            song_id = song_id[song_id.index("/")+1:]
        if "P" in song_id:
            song_id = song_id[song_id.index("P")+1:]
        return self._api.getSong(token, song_id)[0]['data'][0]


    def get_stream_url(self, token, song_id):
        if "/" in song_id:
            song_id = song_id[song_id.index("/")+1:]
        if "P" in song_id:
            song_id = song_id[song_id.index("P")+1:]
        try:
            return self._api.getTrack(token, song_id)['location'].replace("http://", "https://")
        except:
            return self._api.getTrack(token, song_id)

    def get_info(self, token, song_id):
        return self._api.getTrack(token, song_id)['duration'], self._api.getTrack(token, song_id)['extras']

    def register_web_play(self, token, songId,  playper, playsecs, extras, playlistid = None):
        return self._api.registerWebPlay(token, songId, playper, playsecs, playlistid)




    def get_thumbnail(self, artist_art):
        return {
            'small':self._api.getThumbnail(artist_art, 800).replace("http://", "https://"),
            'large':self._api.getThumbnail(artist_art, 1856).replace("http://", "https://")
            }
        # return {
        #     'small':'https://s3-eu-west-1.amazonaws.com/anghami.intern/diaa/small.jpeg',
        #     'large':'https://s3-eu-west-1.amazonaws.com/anghami.intern/diaa/large.jpeg'
        #     }

    def get_downloads(self, token):
        playlists= self._api.getPlaylists(token)[0]['data']
        for val in playlists:
            if val['name'] == '$1234567890DOWNLOADED':
                return val['id'], list(reversed(self._api.getPlaylistData(token, val['id'])))
        return False, False


    def get_likes(self, token):
        playlists = self._api.getPlaylists(token)[0]['data']
        for val in playlists:
            if val['name'] == '$1234567890LIKED#':
                return val['id'], list(reversed(self._api.getPlaylistData(token, val['id'])))
        return False, False

    def get_playlist(self, token, playlist_name):
        playlists = self._api.getPlaylists(token)[0]['data']
        for index, val in enumerate(playlists):
            if playlist_name.lower() == playlists[index]['name'].lower():
                return playlists[index]['id'], self._api.getPlaylistData(token, playlists[index]['id'])
        return False, False


    def get_new_songs(self, token, language = None):
        if language is not None:
            if language.lower() == 'arabic':
                language = 'Arabic'
                return language, self._api.getGenericContent(token, language = 1)
            elif language.lower() == 'international':
                language = 'International'
                return language, self._api.getGenericContent(token, language = 2)
            else:
                language = None
                return language, self._api.getGenericContent(token, language = 0)
        else:
                return language, self._api.getGenericContent(token, language = 0)

    def extract_track_info(self, track):
        return (track,track['id'])

    def get_artist_album_list(self, token, artist_name):
        artist_info = self.get_artist(token, artist_name)
        album_info = artist_info['sections'][2]['data']

        album_list_text = "Here's the album listing for %s: " % artist_name

        for index, val in enumerate(album_info):
            if index > 25:  # alexa will time out if the list takes too long to iterate through
                break
            album_list_text += (album_info[index]['title']) + ", "
        return album_list_text

    def get_my_radios(self, token):
        return self._api.getRadios(token)

    def get_random_radio(self, token, data = []):
        radios = []
        for val in self._api.getRadios(token)['sections']:
            radios += val['data']
        random.shuffle(radios)
        radio = radios[0]
        return self._api.getRadioData(token, radio['type'], radio['id'], data)


    def get_radio_by_name(self, token, radio_name, data = []):
        tags = self._api.getDisplayTags(token)
        for tag in tags:
            if tag['name'] == radio_name:
                return self._api.getRadioData(token, "tag", tag['id'], data)
        tag_id = self.get_tag_id(token, radio_name)
        if tag_id is not False:
            return self._api.getRadioData(token, "tag", tag_id, data)

        search = self._search(token, "artist", radio_name)

        if len(search) == 0:
            return False

        artist_id = search[0]['id']
        return self._api.getRadioData(token, "artist", artist_id, data)




    def closest_match(self, request_name, all_matches, artist_name='', minimum_score=70):
        # Give each match a score based on its similarity to the requested
        # name
        self.logger.debug("The artist name is " + str(artist_name))
        request_name = request_name.lower() + artist_name.lower()
        scored_matches = []
        for i, match in enumerate(all_matches):
            try:
                name = match['name'].lower()
            except (KeyError, TypeError):
                i = match
                name = all_matches[match]['title'].lower()
                if artist_name != "":
                    name += all_matches[match]['artist'].lower()

            scored_matches.append({
                'index': i,
                'name': name,
                'score': fuzz.ratio(name, request_name)
            })

        sorted_matches = sorted(scored_matches, key=lambda a: a['score'], reverse=True)
        top_scoring = sorted_matches[0]
        self.logger.debug("The top scoring match was: " + str(top_scoring))
        best_match = all_matches[top_scoring['index']]

        # Make sure we have a decent match (the score is n where 0 <= n <= 100)
        if top_scoring['score'] < minimum_score:
            return None

        return best_match


    def get_song_data(self, token, song_id):
        return self._api.getSong(token, song_id)


    @classmethod
    def generate_api(cls, **kwargs):
        return cls(**kwargs)
